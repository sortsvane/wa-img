package worker

import (
	"flag"
	"fmt"
	"mime"
	"os"
	"regexp"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"

	"go.mau.fi/whatsmeow"
	"go.mau.fi/whatsmeow/types"
	"go.mau.fi/whatsmeow/types/events"
	waLog "go.mau.fi/whatsmeow/util/log"
)

func extractMobileNumber(input string) string {
	re := regexp.MustCompile(`(\d+)`)
	match := re.FindStringSubmatch(input)
	if len(match) > 1 {
		return match[0]
	}
	return ""
}

func parseJID(arg string) (types.JID, bool) {
	arg = extractMobileNumber(arg)
	if arg[0] == '+' {
		arg = arg[1:]
	}
	if !strings.ContainsRune(arg, '@') {
		return types.NewJID(arg, types.DefaultUserServer), true
	} else {
		recipient, err := types.ParseJID(arg)
		if err != nil {
			return recipient, false
		} else if recipient.User == "" {
			return recipient, false
		}
		return recipient, true
	}
}

func HandleEvent(evt *events.Message, cli *whatsmeow.Client, log waLog.Logger) {
	msgSrc := extractMobileNumber(evt.Info.Chat.String())
	folderLocation := flag.String("location", "images", "Where to save the images ?")
	group := flag.String("group", "", "Get the group id")
	flag.Parse()
	if *group == "" {
		fmt.Println(msgSrc)
		fmt.Println(`Copy the above code (this is your group id) ! and rerun the script using ./script.ext -group="the-code-above"`)
	} else {
		if *group == msgSrc {
			img := evt.Message.GetImageMessage()
			if img != nil {
				data, err := cli.Download(img)
				if err != nil {
					log.Errorf("Failed to download image: %v", err)
					return
				}
				t := time.Now()
				ts := t.Format("2006-01-02 15:04:05")
				exts, _ := mime.ExtensionsByType(img.GetMimetype())
				path := fmt.Sprintf(*folderLocation+"/%s%s", ts, exts[0])
				err = os.WriteFile(path, data, 0600)
				if err != nil {
					log.Errorf("Failed to save image: %v", err)
					return
				}
				log.Infof("Saved image in message to %s", path)
			}
		} else {
			log.Errorf("Group id mismatch")
		}
	}
}
