## WaImg
### Save all images received in a particular group on WhatsApp.
Uses unofficial implementation if WhatsApp protocol.

### Using WaImg

 1. Clone this repository.
	 `git clone https://gitlab.com/sortsvane/wa-img`
	 
2. Go to releases folder.
	cd releases

3. Execute the script using default parameters.
	Unix : `$ chmod +x ./waimg-amd64-linux ; ./waimg-amd64-linux`
	Windows(powershell): `./waimg-amd64.exe`

4. Scan the QR code using WhatsApp > Add Device

5. Wait for a message in the group and copy the group ID.

6. Re-Run the script using flag `-group="your group id"`
	eg.	`./waimg-amd64.exe -group="group-id"`

7. By default the images are stored in `images` folder. To modify the path use `-location` flag.
	eg. `./waimg-amd64.exe -group="group-id" -location="path-to-folader-without-'/'-in-the-end"`
